import math
import numpy as np
import h5py
#import matplotlib.pyplot as plt
import tensorflow as tf
#import time
#from sklearn.model_selection import train_test_split

from tensorflow.python.framework import ops

#from cnn_utils import *



def create_placeholders(n_H0, n_W0, n_C0, n_y):
    """
    Creates the placeholders for the tensorflow session.
    
    Arguments:
    n_H0 -- scalar, height the spectrogram (i.e. frequency bins/bands)
    n_W0 -- scalar, width of the spectrogram (i.e. time frames)
    n_C0 -- scalar, number of channels of the input
    n_y -- scalar, number of classes
        
    Returns:
    X -- placeholder for the data input, of shape [None, n_H0, n_W0] and dtype "float"
    Y -- placeholder for the input labels, of shape [None, n_y] and dtype "float"
    """
    with tf.name_scope('Placeholder'):
        X = tf.placeholder(tf.float32, [None, n_H0, n_W0, n_C0], name = 'X')
        Y = tf.placeholder(tf.float32, [None, n_y], name = 'Y')
    #tf.add_to_collection('Placeholder_X', X)
    #tf.add_to_collection('Placeholder_Y', Y)
    
    return X, Y


def forward_propagation(X):
    """
    Implements the forward propagation for the model:
    CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> FULLYCONNECTED
    -> RELU -> SOFTMAX
    
    Arguments:
    X -- input dataset placeholder, of shape (number of examples, input size)


    Returns:
    Z5 -- the output of the last layer
    parameters -- python dictionary containing the parameters "W1", "W2", "W3", "b1", "b2", "b3"

    """
    
    with tf.variable_scope('Conv1'):
        W1 = tf.get_variable("W", [5,5,1,24], initializer=tf.contrib.layers.xavier_initializer())   
        b1 = tf.get_variable('b', shape = [24], initializer = tf.zeros_initializer)
        conv1 = tf.nn.conv2d(X, W1, strides=[1, 1, 1, 1], padding='VALID', data_format = 'NHWC')
        Z1 = tf.nn.bias_add(conv1, b1)
        A1 = tf.nn.relu(Z1)
        P1 = tf.nn.max_pool(A1, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding='VALID')
        tf.summary.histogram("weights", W1)
        tf.summary.histogram("biases", b1)

    with tf.variable_scope('Conv2'):
        W2 = tf.get_variable("W", [5,5,24,48], initializer=tf.contrib.layers.xavier_initializer())
        b2 = tf.get_variable('b', shape = [48], initializer = tf.zeros_initializer)
        conv2 = tf.nn.conv2d(P1, W2, strides=[1, 1, 1, 1], padding='VALID')
        Z2 = tf.nn.bias_add(conv2, b2)
        A2 = tf.nn.relu(Z2)
        P2 = tf.nn.max_pool(A2, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding='VALID')
        tf.summary.histogram("weights", W2)
        tf.summary.histogram("biases", b2)

    with tf.variable_scope('Conv3'):
        W3 = tf.get_variable("W", [5,5,48,48], initializer=tf.contrib.layers.xavier_initializer())
        b3 = tf.get_variable('b', shape = [48], initializer = tf.zeros_initializer)
        conv3 = tf.nn.conv2d(P2, W3, strides=[1, 1, 1, 1], padding='VALID')
        Z3 = tf.nn.bias_add(conv3, b3)
        A3 = tf.nn.relu(Z3)
        P3 = tf.nn.max_pool(A3, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding='VALID')
        P3_shape = P3.get_shape().as_list()
        P = tf.reshape(P3, [-1, P3_shape[1]*P3_shape[2]*P3_shape[3]])
        tf.summary.histogram("weights", W3)
        tf.summary.histogram("biases", b3)
        
 
    # FULLY-CONNECTED with 64 neurons in output layer and ReLU activation.
    with tf.variable_scope('FC1'):
        W4 = tf.get_variable("W", [2304,64], initializer=tf.contrib.layers.xavier_initializer())
        b4 = tf.get_variable('b', shape = [64], initializer = tf.zeros_initializer)
        Z4 = tf.nn.relu(tf.matmul(P,W4)+b4)
        
    
    #FULLY-CONNECTED with 10 neurons in output layer
    with tf.variable_scope('FC2'):
        W5 = tf.get_variable("W", [64,10], initializer=tf.contrib.layers.xavier_initializer())
        b5 = tf.get_variable('b', shape = [10], initializer = tf.zeros_initializer)
        Z5 = tf.matmul(Z4,W5)+b5

    parameters = {"W1": W1, "W2": W2, "W3": W3, "W4": W4, "W5": W5, 
                  "b1": b1, "b2":b2, "b3":b3, "b4": b4, "b5": b5}
    

    return Z5, parameters



def forward_propagation_with_dropout(X, keep_prob):
    """
    Implements the forward propagation for the model:
    CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> FULLYCONNECTED
    -> RELU -> SOFTMAX
    
    Arguments:
    X -- input dataset placeholder, of shape (input size, number of examples)
    keep_prob -- the probability for neurons of certain layer to be kept, used for dropout regularization

    Returns:
    Z5 -- the output of the last layer
    parameters -- python dictionary containing the parameters "W1", "W2", "W3", "b1", "b2", "b3"
    """
    with tf.variable_scope('Conv1'):
        W1 = tf.get_variable("W", [5,5,1,24], initializer=tf.contrib.layers.xavier_initializer())   
        b1 = tf.get_variable('b', shape = [24], initializer = tf.zeros_initializer)
        conv1 = tf.nn.conv2d(X, W1, strides=[1, 1, 1, 1], padding='VALID', data_format = 'NHWC')
        Z1 = tf.nn.bias_add(conv1, b1)
        A1 = tf.nn.relu(Z1)
        P1 = tf.nn.max_pool(A1, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding='VALID')
        tf.summary.histogram("weights", W1)
        tf.summary.histogram("biases", b1)

    with tf.variable_scope('Conv2'):
        W2 = tf.get_variable("W", [5,5,24,48], initializer=tf.contrib.layers.xavier_initializer())
        b2 = tf.get_variable('b', shape = [48], initializer = tf.zeros_initializer)
        conv2 = tf.nn.conv2d(P1, W2, strides=[1, 1, 1, 1], padding='VALID')
        Z2 = tf.nn.bias_add(conv2, b2)
        A2 = tf.nn.relu(Z2)
        P2 = tf.nn.max_pool(A2, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding='VALID')
        tf.summary.histogram("weights", W2)
        tf.summary.histogram("biases", b2)

    with tf.variable_scope('Conv3'):
        W3 = tf.get_variable("W", [5,5,48,48], initializer=tf.contrib.layers.xavier_initializer())
        b3 = tf.get_variable('b', shape = [48], initializer = tf.zeros_initializer)
        conv3 = tf.nn.conv2d(P2, W3, strides=[1, 1, 1, 1], padding='VALID')
        Z3 = tf.nn.bias_add(conv3, b3)
        A3 = tf.nn.relu(Z3)
        P3 = tf.nn.max_pool(A3, ksize = [1, 2, 2, 1], strides = [1, 2, 2, 1], padding='VALID')
        P3_shape = P3.get_shape().as_list()
        tf.summary.histogram("weights", W3)
        tf.summary.histogram("biases", b3)
        with tf.variable_scope('Flatten'):
            P = tf.reshape(P3, [-1, P3_shape[1]*P3_shape[2]*P3_shape[3]])
        
    P_drop1 = tf.nn.dropout(P, keep_prob)
        
    with tf.variable_scope('FC1'):
        W4 = tf.get_variable("W", [2304,64], initializer=tf.contrib.layers.xavier_initializer())
        b4 = tf.get_variable('b', shape = [64], initializer = tf.zeros_initializer)
        Z4 = tf.nn.relu(tf.matmul(P_drop1,W4)+b4)
        
    P_drop2 = tf.nn.dropout(Z4, keep_prob)
        
    with tf.variable_scope('FC2'):
        W5 = tf.get_variable("W", [64,10], initializer=tf.contrib.layers.xavier_initializer())
        b5 = tf.get_variable('b', shape = [10], initializer = tf.zeros_initializer)
        Z5 = tf.matmul(P_drop2,W5)+b5
    

    parameters = {"W1": W1, "W2": W2, "W3": W3, "W4": W4, "W5": W5, 
                  "b1": b1, "b2":b2, "b3":b3, "b4": b4, "b5": b5}


    return Z5, parameters



def compute_cost(Z5, Y):
    """
    Computes the cost
    
    Logits and labels must have the same shape, e.g. [batch_size, num_classes] 
    
    Arguments:
    Z5 -- output of forward propagation (without softmax function), of shape (number of examples,10)
    Y -- "true" labels vector placeholder, same shape as Z5
    
    Returns:
    cost - Tensor of the cost function
    """
    with tf.name_scope('cross_entropy'):
       
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=Z5, labels=Y))
        tf.summary.scalar('cost', cost)  
    
    return cost



def compute_cost_with_regularization(Z5, Y, parameters, lambd):
    """
    Computes the cost
    
    Logits and labels must have the same shape, e.g. [batch_size, num_classes] 
    
    Arguments:
    Z5 -- output of forward propagation (without softmax function), tensor of shape (number of examples,10)
    Y -- "true" labels vector placeholder, tensor with the same shape as Z5
    parameters -- dictionary containing the values of the filters' weights
    lambd -- regularization hyperparameter
    
    Returns:
    cost - Tensor of the cost function
    """   
    #W1 = parameters["W1"]
    #W2 = parameters["W2"]
    #W3 = parameters["W3"]
    FC1_W = parameters["W4"]
    FC2_W = parameters["W5"]
    
    with tf.name_scope('Cost'):
        
        with tf.name_scope('cross_entropy'):
            cross_entropy_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=Z5, labels=Y))
            # This gives us the cross-entropy part of the cost
            
        with tf.name_scope('L2_regularization'):
            m = tf.size(Y, out_type = tf.int32)
            m = tf.cast(m, dtype = tf.float32)
            # Loss function using L2 Regularization;   tf.nn.l2_loss already divides by two and squares the weights
            # regularizer = tf.nn.l2_loss(W1)+tf.nn.l2_loss(W2)+tf.nn.l2_loss(W3)+tf.nn.l2_loss(FC1_W) + tf.nn.l2_loss(FC2_W)
            regularizer = tf.nn.l2_loss(FC1_W) + tf.nn.l2_loss(FC2_W) 
            regularizer = tf.divide(regularizer,m) # we then divide by the number of examples
            
        with tf.name_scope('total_cost'):
            cost = cross_entropy_loss + tf.multiply(lambd, regularizer) # we multiply by the regularization hyperparm and add to the loss
        
        tf.summary.scalar('cost', cost)
          
    return cost






